const loadsController = require("../Controllers/loadsController");
const router = require("express").Router();
const authMiddleWare = require('../middleWare/authMiddleWare')
const roleMiddleWare = require('../middleWare/roleMiddleWare')

router.get("/loads",authMiddleWare, loadsController.getLoads) 
router.post("/loads",authMiddleWare,roleMiddleWare.checkRoleShipper, loadsController.addLoad)
router.get("/loads/active",authMiddleWare,roleMiddleWare.checkRoleDriver, loadsController.getActiveLoad)
router.patch("/loads/active/state",authMiddleWare,roleMiddleWare.checkRoleDriver, loadsController.nextState)
router.get("/loads/:id",authMiddleWare, loadsController.getLoadById)
router.put("/loads/:id",authMiddleWare,roleMiddleWare.checkRoleShipper, loadsController.updateLoadById)
router.delete("/loads/:id",authMiddleWare,roleMiddleWare.checkRoleShipper, loadsController.deleteLoadById)
router.post("/loads/:id/post",authMiddleWare,roleMiddleWare.checkRoleShipper, loadsController.postLoadById)
router.get("/loads/:id/shipping_info",authMiddleWare,roleMiddleWare.checkRoleShipper, loadsController.shippingInfoById)

module.exports = router;
