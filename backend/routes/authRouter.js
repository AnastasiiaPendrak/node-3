const router = require("express").Router();
const { check } = require("express-validator");
const authController = require("../Controllers/authController");



router.post(
  "/auth/register",
  [
    check("email", "This input can not be empty").notEmpty(),
    check("password", "The password must contain 3-10 numbers").isLength({
      min: 3,
      max: 10,
    }),
  ],
  authController.registration
);


router.post("/auth/login", authController.login)
router.post("/auth/forgot_password", authController.forgotPassword)



module.exports = router;
