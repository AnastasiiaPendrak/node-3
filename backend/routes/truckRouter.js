const truckController = require("../Controllers/truckController");
const router = require("express").Router();
const roleMiddleWare = require('../middleWare/roleMiddleWare')
const authMiddleWare = require('../middleWare/authMiddleWare')

router.get("/trucks",authMiddleWare, roleMiddleWare.checkRoleDriver, truckController.getTrucks)
router.post("/trucks",authMiddleWare, roleMiddleWare.checkRoleDriver, truckController.addTruck)
router.get("/trucks/:id",authMiddleWare,roleMiddleWare.checkRoleDriver, truckController.getTruckById)
router.put("/trucks/:id",authMiddleWare,roleMiddleWare.checkRoleDriver, truckController.updateTruckById)
router.delete("/trucks/:id",authMiddleWare,roleMiddleWare.checkRoleDriver, truckController.deleteTruckById)
router.post("/trucks/:id/assign",authMiddleWare, roleMiddleWare.checkRoleDriver, truckController.assignTruckById)


module.exports = router;
