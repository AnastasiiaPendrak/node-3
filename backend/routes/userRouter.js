const userController = require("../Controllers/userController");
const router = require("express").Router();
const authMiddleWare = require('../middleWare/authMiddleWare')

router.get("/users/me", authMiddleWare, userController.getProfileInfo)
router.delete("/users/me",authMiddleWare, userController.deleteUser)
router.patch("/users/me/password", userController.changePassword)

module.exports = router;
