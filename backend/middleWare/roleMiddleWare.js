const User = require("../models/User");

const checkRoleDriver = async (req, res, next) => {
  const currentRole = await req.user.role;

  if (currentRole === "SHIPPER") {
    return await res.status(500).json({
      message: `You have not permission. You are a shipper`,
    });
  }

  next();
};

const checkRoleShipper = async (req, res, next) => {
  const currentRole = await req.user.role;

  if (currentRole === "DRIVER") {
    return await res.status(500).json({
      message: `You have not permission. You are a driver`,
    });
  }

  next();
};

module.exports = {
  checkRoleDriver,
  checkRoleShipper,
};
