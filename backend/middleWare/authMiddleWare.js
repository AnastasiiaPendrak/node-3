const jwt = require("jsonwebtoken");
const { secret } = require("../config");

module.exports = function (req, res, next) {
  if (req.method === "OPTIONS") {
    next();
  }

  try {
    const token = req.headers.authorization.split(" ")[1];
    // const token = req.headers.authorization

    if (!token) {
      return res.status(400).json({
        message: "The author is not authorized",
      });
    }

    const decodedData = jwt.verify(token, secret);

    req.user = decodedData;

    next();
  } catch (error) {
    console.log(error);
    return res.status(400).json({
      message: "ERROR",
    });
  }
};
