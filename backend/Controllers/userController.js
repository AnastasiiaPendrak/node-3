const User = require("../models/User");
const bcrypt = require("bcryptjs");
const { any } = require("joi");

class userController {
  async getProfileInfo(req, res) {
    try {
      const currentId = await req.user.id;
      const users = await User.findOne({ _id: currentId });

      const user = {
        _id: users._id,
        role: users.role,
        email: users.email,
        createdDate: users.createdAt,
      };

      if (!users) {
        return res.status(400).json({
          message: "User not found",
        });
      }

      if (users) {
        return res.status(200).json({
          user,
        });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async deleteUser(req, res) {
    try {
      const currentId = await req.user.id;
      const users = await User.findOne({ _id: currentId });

      if (!users) {
        return res.status(400).json({
          message: "Error",
        });
      }

      if (users) {
        await User.findByIdAndDelete(users._id);

        return res.status(200).json({
          message: "Profile deleted successfully",
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async changePassword(req, res) {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const users = await User.findOne(req.header._id);
    const validPassword = bcrypt.compareSync(oldPassword, users.password);

    try {
      if (users) {
        if (!validPassword) {
          return res.status(400).json({
            message: `Check your password`,
          });
        }

        const hashPassword = bcrypt.hashSync(newPassword, 7);
        users.password = hashPassword;
        await users.save();

        return res.status(200).json({
          message: "Password changed successfully",
        });
      } else {
        return res.status(400).json({
          message: "User not found",
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Error",
      });
    }
  }
}

module.exports = new userController();
