const User = require("../models/User");
const Truck = require("../models/Truck");
const jwt = require("jsonwebtoken");
const { secret } = require("../config");

class truckController {
  async getTrucks(req, res) {
    try {
      const trucks = await Truck.find();

      if (!trucks) {
        return res.status(400).json({
          message: "Error",
        });
      }

      res.status(200).json({
        trucks,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async addTruck(req, res) {
    const { type } = req.body;

    const user = await User.findOne();
    const truck = await User.findOne();

    if (!user) {
      return res.status(400).json({
        message: "Error",
      });
    }

    const newTruck = new Truck({
      _id: truck.id,
      type,
      created_by: user._id,
    });

    await newTruck.save();

    return res.status(200).json({
      message: "Truck created successfully",
    });
  }

  async getTruckById(req, res) {
    try {
      const truck = await Truck.findById(req.params.id);

      if (!truck) {
        return res.status(400).json({
          message: "Error",
        });
      }

      return res.status(200).json({
        truck,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async updateTruckById(req, res) {
    try {
      const updatedType = req.body.type;

      const truck = await Truck.findOne({
        _id: req.params.id,
        status: "IS",
      });



      truck.type = updatedType;
      truck.save();

      if (!updatedType) {
        return res.status(400).json({
          message: "Error",
        });
      }

      if (!truck) {
        return await res.status(400).json({
          message: `Truck's status is OL!`,
        });
      }

      return res.status(200).json({
        message: "Truck details changed successfully",
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        message: "Error",
      });
    }
  }

  async deleteTruckById(req, res) {
    try {
      const result = await Truck.findByIdAndDelete(req.params.id);

      if (!result) {
        return res.status(400).json({
          message: "Error",
        });
      }
      return res.status(200).json({
        message: "Success",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async assignTruckById(req, res) {
    try {
      const truck = await Truck.findOne({ _id: req.params.id, status: "IS" });
      const user = await User.findOne();

      if (!truck) {
        return await res.status(400).json({
          message: `Truck not found!`,
        });
      }

      truck.assigned_to = user._id;
      await truck.save();

      const checkOL = await Truck.findOne({
        assigned_to: user._id,
        status: "OL",
      });

      if (checkOL) {
        return await res.status(400).json({
          message: `This truck is OL!`,
        });
      }

      await res.status(200).json({
        message: `Truck assigned successfully`,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }
}

module.exports = new truckController();
