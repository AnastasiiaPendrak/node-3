const Load = require("../models/Load");
const Truck = require("../models/Truck");
const User = require("../models/User");
const { LOAD_STATES } = require("../utils/states_types");

class loadsController {
  async getLoads(req, res) {
    try {
      const user = await User.findOne(req.header._id);
      const limit = req.query.limit;
      const offset = req.query.offset;
      const myStatus = req.query.status;
      const allvalues = {};

      if (myStatus) {
        allvalues.status = myStatus;
      }

      const loads = await Load.find(allvalues).skip(offset).limit(limit);

      if (!loads) {
        await res.status(400).json({
          message: `Loads not found!`,
        });
      }

      await res.json({
        loads,
      });
    } catch (error) {

      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async addLoad(req, res) {
    try {
      const user = await User.findOne();

      const load = new Load({
        ...req.body,
        _id: user._id,
        created_by: user._id,
        logs: [{ message: "Load created." }],
        time: Date.now(),
      });

      if (!user) {
        return res.status(400).json({
          message: "Error",
        });
      }

      await load.save();

      return res.status(200).json({
        message: "Load created successfully",
      });
    } catch (error) {
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async getActiveLoad(req, res) {
    try {
      const load = await Load.findOne({ status: "ASSIGNED" });

      if (!load) {
        return await res.status(400).json({
          message: `You have not active loads`,
        });
      }

      return res.status(200).json({
        load,
      });
    } catch (error) {
    
      return await res.status(500).json({
        message: `Error`,
      });
    }
  }

  async nextState(req, res) {
    try {
      const user = await User.findOne();
      const load = await Load.findOne();

      if (load.status !== "ASSIGNED") {
        return await res.status(400).json({
          message: `Load status is not assigned`,
        });
      }

      for (let i = 0; i < LOAD_STATES.length; i++) {
        load.state = LOAD_STATES[i];
      }

      if (!load) {
        return await res.status(400).json({
          message: `The active load not found!`,
        });
      }



      if (load.state === "Arrived to delivery") {
        const truck = await Truck.findOne({ assigned_to: user._id, status: 'ASSIGNED' });
        truck.status = "IS";
        load.status = "SHIPPED";
        await truck.save();
      }

      await load.save();

      return res.status(200).json({
        message: `Load state changed to ${load.state} `,
      });
    } catch (error) {
 
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async getLoadById(req, res) {
    try {
      const load = await Load.findById(req.params.id);

      if (!load) {
        return res.status(400).json({
          message: "Error",
        });
      }

      return res.status(200).json({
        load,
      });
    } catch (error) {

      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async updateLoadById(req, res) {
    try {
      const U_name = req.body.name;
      const U_payload = req.body.payload;
      const U_pickup_address = req.body.pickup_address;
      const U_delivery_address = req.body.delivery_address;
      const U_dimensions = req.body.dimensions;

      const load = await Load.findOne({ _id: req.params.id });

      load.name = U_name;
      load.payload = U_payload;
      load.pickup_address = U_pickup_address;
      load.delivery_address = U_delivery_address;
      load.dimensions = U_dimensions;

      load.save();

      if (!load) {
        return await res.status(400).json({
          message: `Error`,
        });
      }

      return res.status(200).json({
        message: "Load details changed successfully",
      });
    } catch (error) {
      res.status(500).json({
        message: "Error",
      });
    }
  }

  async deleteLoadById(req, res) {
    try {
      const result = await Load.findByIdAndDelete(req.params.id);

      if (!result) {
        return res.status(400).json({
          message: "Error",
        });
      }
      return res.status(200).json({
        message: "Load deleted successfully",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async postLoadById(req, res) {
    try {
      const user = await User.findOne();
      const load = await Load.findOne({ status: "NEW" });

      if (!load) {
        return await res.status(400).json({
          message: `The load not found!`,
        });
      }

      load.status = "POSTED";

      await load.save();

      return await res.status(200).json({
        message: "Load posted successfully",
        driver_found: true,
      });
    } catch (error) {

      res.status(500).json({
        message: `Error`,
      });
    }
  }

  async shippingInfoById(req, res) {
    try {
      const load = await Load.findById(req.params.id);
      const truck = await Truck.findById(req.params.id);

      if (!load) {
        return res.status(400).json({
          message: "Error",
        });
      }

      return res.status(200).json({
        load,
        // createdDate: load.created_date,
        truck,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }
}

module.exports = new loadsController();
