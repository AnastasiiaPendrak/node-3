const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const { secret } = require("../config");
const User = require("../models/User");

const generateAccessToken = (id, role) => {
  const payload = { id, role };

  const accessToken = jwt.sign(
    payload,
    // { userRole: roles },
    // { userId: id },
    secret,
    { expiresIn: "24h" }
  );

  return accessToken;
};

class authController {
  async registration(req, res) {
    try {
      const errors = validationResult(req.body);
      if (!errors.isEmpty()) {
        return res.status(400).json({
          message: "Your password is not correct",
          errors,
        });
      }

      const { email, password, role } = req.body;

      const candidate = await User.findOne({ email });
      if (candidate) {
        return res.status(400).json({
          message: "email already exists",
        });
      }
      const hashPassword = bcrypt.hashSync(password, 10);
      const user = new User({
        email,
        password: hashPassword,
        role,
      });

      await user.save();

      return res.status(200).json({
        message: "Profile created successfully",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async login(req, res) {
    try {
      const { email, password } = req.body;

      const user = await User.findOne({ email });

      if (!user) {
        return res.status(400).json({
          message: `User with such email - ${email} is not exist`,
        });
      }

      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({
          message: `Check your password`,
        });
      }

      const token = generateAccessToken(user._id, user.role);

      return res.status(200).json({
        jwt_token: token,
      });
      
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }

  async forgotPassword(req, res) {
    try {
      const { email } = req.body;
      const user = await User.findOne({ email });

      if (!user) {
        return res.status(400).json({
          message: `User with such email - ${email} is not exist`,
        });
      }

      return res.status(200).json({
        message: "New password sent to your email address",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Error",
      });
    }
  }
}

module.exports = new authController();
