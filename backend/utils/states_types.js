
  const LOAD_STATES = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
  ];
  

const TRUCK_TYPES = [
    {
      type: 'SPRINTER',
      payload: 1700,
      dimensions: {
        width: 300,
        length: 250,
        height: 170,
      },
    },
    {
      type: 'SMALL STRAIGHT',
      payload: 2500,
      dimensions: {
        width: 500,
        length: 250,
        height: 170,
      },
    },
    {
      type: 'LARGE STRAIGHT',
      payload: 4000,
      dimensions: {
        width: 700,
        length: 350,
        height: 170,
      },
    },
  ];
  

  module.exports = {
    TRUCK_TYPES,
    LOAD_STATES,
  };
  