const express = require("express");
const dbPassword = "5E9vld3mVGGmfSUf";
const mongoose = require("mongoose");

const authRouter = require("../backend/routes/authRouter");
const userRouter = require("../backend/routes/userRouter");
const truckRouter = require("../backend/routes/truckRouter");
const loadsRouter = require("../backend/routes/loadsRouter");

const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");
const YAML = require("yamljs");
const process = require("process");
require("dotenv").config(".env");

const PORT = process.env.PORT || 8080;
const cors = require("cors");

const app = express();

const swaggerDocument = YAML.load("./openapi.yaml");

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Library API",
      version: "1.0.0",
      description: "A simple Express Library API",
    },
    servers: [
      {
        url: "http://localhost:8080",
      },
    ],
  },
  apis: ["./routes/*.js"],
};
swaggerJsDoc(options);

app.use(express.json());
app.use(cors());
app.use("/api", authRouter, userRouter, truckRouter, loadsRouter);

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

const start = async () => {
  try {
    const connectionParams = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };

    await mongoose.connect(process.env.DB, connectionParams);
    app.listen(PORT, () => {
      console.log(`server started on port ${PORT}`);
    });

    app.listen(80, function () {
      console.log("CORS-enabled web server listening on port 80");
    });
  } catch (error) {
    console.log(error);
  }
};
start();
