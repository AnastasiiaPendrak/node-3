const { Schema, model } = require("mongoose");

const Truck = new Schema({
  _id: { type: String },
  created_by: { type: String, required: true },
  assigned_to: { type: String, default: null},
  type: {
    type: String,
    required: true,
    enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"],
  },
  status: { type: String, enum: ["OL", "IS"], default: "IS" },
  created_date: { type: String, default: new Date().toLocaleDateString() },
});

module.exports = model("Truck", Truck);
