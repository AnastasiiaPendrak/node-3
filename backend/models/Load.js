const { Schema, model } = require("mongoose");

const Load = new Schema({
  _id: { type: String, required: true },
  created_by: { type: String, required: true },
  assigned_to: { type: String, default: null },
  status: {
    type: String,
    default: "NEW",
    enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
    required: true,
  },
  state: {
    type: String,
    require: true,
    default: null,
    enum: [
      null,
      "En route to Pick Up",
      "Arrived to Pick Up",
      "En route to delivery",
      "Arrived to delivery",
    ],
  },

  name: { type: String, required: true },
  payload: { type: Number, required: true },
  pickup_address: { type: String, required: true },
  delivery_address: { type: String, required: true },

  dimensions: {
    width: { type: Number, required: true },
    length: { type: Number, required: true },
    height: { type: Number, required: true },
  },

  logs: [
    {
      message: {
        type: String,
        require: true,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    },
  ],

  created_date: { type: String, default: new Date().toLocaleDateString() },
});

module.exports = model("Load", Load);
