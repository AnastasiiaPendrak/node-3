const { Schema, model } = require("mongoose");

const User = new Schema(
  {
    email: {
      type: String,
      required: true,
      lowercase: true,
      unique: true,
    },

    password: {
      type: String,
      required: true,
    },

    role: {
      type: String,
      require: true,
      enum: ['SHIPPER', 'DRIVER'],
  
      },
  
    resetLink: {
      data: String,
      default: "",
    },
  },
  { timestamps: true }
);




module.exports = model("User", User);
